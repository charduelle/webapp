// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressSession = require('express-session');
var mongoose   = require('mongoose');
var hash = require('bcrypt-nodejs');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var RememberMeStrategy = require('passport-remember-me').Strategy;
var debug = require('debug')('passport-mongo');
var flash = require('express-flash');
var Token = require('./server/models/token');
var utils = require('./server/utils/auth');

var User = require('./server/models/user');
var authRoutes = require('./server/routes/auth')(app, express);
var apiRoutes = require('./server/routes/api')(app, express);
var clientRoutes = require('./server/routes/client')(app, express);

var config = require('./server/config.js');

var port = config.port();

// Connect to mongodb
mongoose.connect('mongodb://selogerAdmin:seloger@ds153715.mlab.com:53715/seloger'); // connect to our database
//mongoose.connect('mongodb://localhost/test'); // connect to our local database
var conn = mongoose.connection;             
conn.on('error', console.error.bind(console, 'connection error:'));

var app        = express();                 

// define middleware
app.use(logger('dev')); // log all activity for debug
app.use(flash()); // for flash error messages
app.use(bodyParser.json()); // let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser()); // let us get data from cookie
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(passport.authenticate('remember-me'));

// configure passport
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


// Remember Me cookie strategy
//   This strategy consumes a remember me token, supplying the user the
//   token was originally issued to.  The token is single-use, so a new
//   token is then issued to replace it.


passport.use(new RememberMeStrategy(
  function(token, done) {
    utils.consumeRememberMeToken(token, function(err, id) {
      if (err) { return done(err); }
      if (!id) { return done(null, false); }
      
      User.findById(id, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        return done(null, user);
      });
    });
  },
  utils.issueToken
));

var isAuthenticated = function (req, res, next) {
  if (req.isAuthenticated())
    return next();
  res.sendStatus(401);
}

// var redirectLoggedUsers = function(req, res, next){
//     if (req.isAuthenticated()) {
//         res.redirect('/apparts');
//         next();
//     } else {
//         next();
//     }
// }
//app.use(redirectLoggedUsers);


// configure app to handle CORS requests
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

// set static files location
// used for requests that our frontend will make
app.use(express.static(__dirname + '/client'));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));

app.use('/auth', authRoutes);
app.use('/client', clientRoutes);

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', isAuthenticated);
app.use('/api', apiRoutes);

// MAIN CATCHALL ROUTE / SEND USERS TO FRONTEND ------------
// has to be registered after API ROUTES

app.get('*', function(req, res){
  res.sendFile(path.join(__dirname+'/client/app/views/index.html'));
});

// error handlers
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res) {
  res.status(err.status || 500);
  res.end(JSON.stringify({
    message: err.message,
    error: {}
  }));
});

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);

// Variabiliser l'environnement pour les urls des appels api dans la partie auth et client)
// Emails au propre.
// 