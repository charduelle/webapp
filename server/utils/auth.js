var Token = require('../models/token');

module.exports = {
  consumeRememberMeToken: function (cookieToken, fn) {
  	Token.findById(cookieToken, function(err, token) {
	    if (err)
	      console.log(err);

	    if (token) {
	      // invalidate the single-use token
	      token.remove(function(err) {
	        if (err)
	          console.log(err);

	        return fn(null, token.user);
	      });
	    } else {
	    	return fn(null, "");
	    }

  	});
  },

  issueToken: function (user, done) {
	  var token = new Token();
	  token.user = user._id;
	  token.save(function(err) {
	    if (err)
	      console.log(err);

	    return done(null, token._id.toString());
	  });
  }
};