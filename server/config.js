module.exports = {
	port: function() {
		return process.env.PORT || 8080;  
	},
	url: function() {
		if (process.env.PORT) { return "http://www.cozy-home.fr" }
		else { return "http://localhost:8080"}
	}
};

