var bodyParser = require('body-parser');
var passport = require('passport');
var Appart = require('../models/appart');
var User = require('../models/user');
var Search = require('../models/search');
var Like = require('../models/like');
var mongoose = require('mongoose');
var async = require('async');
var crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer');
var config = require('../config.js');
var baseURL = config.url();

module.exports = function(app, express) {

  // ROUTES FOR OUR API
  // =============================================================================
  var apiRouter = express.Router(); // get an instance of the express Router

  // middleware to use for all requests
  apiRouter.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
  });

  // ---------------------------------------------------------- USER ROUTES -------------------------------------

  apiRouter.route('/users')

  .get(function(req, res) {

    User.find(function(err, users) {
      if (err)
        res.send(err);
      res.json(users);
    });
  });

  apiRouter.route('/users/:user_id')

  .get(function(req, res) {
    User.findById(req.params.user_id, function(err, user) {
      if (err)
        res.send(err);
      res.json(user);
    });
  })

  .post(function(req, res) {

    // use our user model to find the user we want
    User.findById(req.params.user_id, function(err, user) {

      if (err)
        res.send(err);

      user.name = req.body.name; // update the user info

      user.save(function(err) {
        if (err)
          res.send(err);

        res.json({
          message: 'User updated!'
        });
      });

    });
  })

  .delete(function(req, res) {

    User.findById(req.params.user_id, function(err, user) {

      if (err)
        res.send(err);

      user.remove(function(err) {
        if (err)
          res.send(err);

        res.json({
          message: 'User was successfully deleted'
        });
      });

    });
  });

  // ---------------------------------------------------------- SEARCH ROUTES -------------------------------------

  apiRouter.route('/searchs')

  .post(function(req, res) {
    User.findOne(req.user).exec(function(err, user) {
      if (err)
        res.send(err);

      var search = new Search();
      search.name = req.body.name;
      search.idtt = req.body.idtt;
      search.idtypebien = req.body.idtypebien;

      // Ce bloc était utilisé pour recréer un array à partir de la string "75001,75002,..." de la version avant multiselect
      // if (req.body.ci) {
      //     ciArray = JSON.parse("[" + req.body.ci + "]");
      //     for (var i = 0; i < ciArray.length; i++) {
      //         search.ci.push(ciArray[i]);
      //     } 
      // }

      if (req.body.ci) {
        for (var i = 0; i < req.body.ci.length; i++) {
          search.ci.push(req.body.ci[i]);
        }
      }

      search.city = req.body.city;

      req.body.surfacemin ? search.surfacemin = req.body.surfacemin : search.surfacemin = 0;
      req.body.surfacemax ? search.surfacemax = req.body.surfacemax : search.surfacemax = 10000;
      req.body.pxmin ? search.pxmin = req.body.pxmin : search.pxmin = 0;
      req.body.pxmax ? search.pxmax = req.body.pxmax : search.pxmax = 10000000;
      req.body.nb_piecesmin ? search.nb_piecesmin = req.body.nb_piecesmin : search.nb_piecesmin = 1;
      req.body.nb_piecesmax ? search.nb_piecesmax = req.body.nb_piecesmax : search.nb_piecesmax = 100;
      req.body.nb_chambresmin ? search.nb_chambresmin = req.body.nb_chambresmin : search.nb_chambresmin = 1;

      search.users.push(user);

      var referer = req.body.referer.name + " " + req.body.referer.surname;
      var usersArray = req.body.users;

      if (usersArray) {

        for (var i = 0; i < usersArray.length; ++i) {
          var username = usersArray[i];

          User.findOne({
            'username': username
          }, function(err, user) {
            if (err) {
              console.log(err);
            }
            if (!user) {
              async.waterfall([
                function(done) {
                  crypto.randomBytes(20, function(err, buf) {
                    var token = buf.toString('hex');
                    done(err, token);
                  });
                },
                function(token, done) {
                  var expireDate = Date.now() + 3600000 * 24 * 7;
                  user = new User({
                    username: username,
                    resetPasswordToken: token,
                    resetPasswordExpires: expireDate
                  });
                  user.save(function(err) {
                    done(err, token, user);
                  });
                },
                function(token, user, done) {
                  search.users.push(user);

                  var transporter = nodemailer.createTransport({
                    service: 'Gmail',
                    auth: {
                      user: 'cozyhome.contacts@gmail.com', // Your email id
                      pass: 'stvznmqmfgcvlmoj' // Your password
                    }
                  });

                  var mailOptions = {
                    from: 'cozyhome.contacts@gmail.com', // sender address
                    to: user.username,
                    subject: 'Vous avez été invité à une recherche sur cozy-home', // Subject line
                    text: 'Bonjour, vous avez été invité à une nouvelle recherche sur cozy-home par ' + referer + '. Vous pourrez la consulter sur www.cozy-home.fr une fois que vous aurez validé votre inscription. Pour terminer votre inscription, veuillez cliquer sur le lien suivant : ' + baseURL + '/auth/confirm-invite/' + token + '.' //, // plaintext body
                      // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
                  };

                  transporter.sendMail(mailOptions, function(err, info) {
                    req.flash('info', 'An e-mail has been sent to ' + user.username + ' with further instructions.');
                    done(err, 'done');
                  });
                }
              ], function(err) {
                if (err) return next(err);
                req.flash('info', 'User has been created and informed');
              });
            } else {

              search.users.push(user);

              var transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                  user: 'cozyhome.contacts@gmail.com', // Your email id
                  pass: 'stvznmqmfgcvlmoj' // Your password
                }
              });

              var mailOptions = {
                from: 'cozyhome.contacts@gmail.com', // sender address
                to: user.username,
                subject: 'Vous avez été invité à une nouvelle recherche', // Subject line
                text: 'Bonjour, vous avez été invité à une nouvelle recherche par ' + referer + '. Vous pouvez la consulter sur www.cozy-home.fr'
                  // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
              };

              transporter.sendMail(mailOptions, function(error, info) {
                if (error) {
                  console.log(error);
                } else {
                  console.log('Message sent: ' + info.response);
                }
              });
            }

          });

        }

      }

      search.save(function(err) {
        if (err) {
          console.log(err);
        }
        res.json({
          message: search
        });
      });

    });
  })

  .get(function(req, res) {
    User.findOne(req.user).populate('searchs').exec(function(err, user) {
      if (err)
        res.send(err);

      User.populate(user, {
        path: 'searchs.users',
        model: 'User'
      }, function(err, data) {
        if (err)
          res.send(err);

        res.json(data.searchs);
      });

    });
  });

  apiRouter.route('/searchs/:search_id')

  .get(function(req, res) {
    Search.findById(req.params.search_id).populate('users').exec(function(err, search) {
      if (err)
        res.send(err);
      res.json(search);
    });
  })

  .post(function(req, res) {

    Search.findById(req.params.search_id, function(err, search) {

      if (err)
        res.send(err);

      if (req.body.name) {
        search.name = req.body.name;
      }
      if (req.body.idtt) {
        search.idtt = req.body.idtt;
      }
      if (req.body.idtypebien) {
        search.idtypebien = req.body.idtypebien;
      }
      if (req.body.city) {
        search.city = req.body.city;
      }
      if (req.body.ci) {
        search.ci = [];
        for (var i = 0; i < req.body.ci.length; i++) {
          search.ci.push(req.body.ci[i]);
        }
      }
      if (req.body.pxmin) {
        search.pxmin = req.body.pxmin;
      }
      if (req.body.pxmax) {
        search.pxmax = req.body.pxmax;
      }
      if (req.body.surfacemin) {
        search.surfacemin = req.body.surfacemin;
      }
      if (req.body.surfacemax) {
        search.surfacemax = req.body.surfacemax;
      }
      if (req.body.nb_piecesmin) {
        search.nb_piecesmin = req.body.nb_piecesmin;
      } //if (req.body.nb_piecesmin) else "";
      if (req.body.nb_piecesmax) {
        search.nb_piecesmax = req.body.nb_piecesmax;
      } //if (req.body.nb_piecesmax) else "";
      if (req.body.nb_chambresmin) {
        search.nb_chambresmin = req.body.nb_chambresmin;
      } //if (req.body.nb_chambresmin) else 1;

      
      search.save(function(err) {
        if (err)
          res.send(err);

          res.json({
            message: 'Search updated!'
          });
        });

    });
  })

  .delete(function(req, res) {
    Search.findById(req.params.search_id, function(err, search) {

      if (err)
        res.send(err);

      search.remove(function(err) {
        if (err)
          res.send(err);

        res.json({
          message: 'Search was successfully deleted'
        });
      });

    });
  });


  apiRouter.route('/searchs/:search_id/toggleStatus')

  .post(function(req, res) {

    Search.findById(req.params.search_id, function(err, search) {

      if (err)
        res.send(err);

      search.status = !search.status;

      search.save(function(err) {
        if (err)
          res.send(err);

        res.json({
          message: search.status ? 'Search was successfully started.' : 'Search was successfully paused.'
        });
      });

    });
  });


  apiRouter.route('/searchs/:search_id/apparts')

  .post(function(req, res) {
    Search.findById(req.params.search_id, function(err, search) {
      if (err)
        res.send(err);

      var appart = new Appart();
      appart.title = req.body.title;
      appart.search = search;

      appart.save(function(err) {
        if (err)
          res.send(err);

        res.json({
          message: appart
        });
      });
    });
  })

  .get(function(req, res) {
    Search.findById(req.params.search_id).populate('apparts').exec(function(err, search) {
      if (err)
        res.send(err);

      res.json(search.apparts);
    });
  });

  // ---------------------------------------------------------- APPART ROUTES -------------------------------------


  apiRouter.route('/apparts')

  .get(function(req, res) {
    User.findOne(req.user).populate('searchs').exec(function(err, user) {
      if (err)
        res.send(err);

      User.populate(user, {
        path: 'searchs.apparts',
        model: 'Appart'
      }, function(err, data) {
        if (err)
          res.send(err);

        User.populate(data, {
          path: 'searchs.apparts.like',
          model: 'Like'
        }, function(err, datas) {
          if (err)
            res.send(err);

          res.send(datas);
        });
      });
    });

  });

  // var appartList = [];
  //                 k = 0;
  //                 for (var i = 0; i < user.searchs.length; i++) {
  //                     if (user.searchs[i].apparts[0])
  //                         for (var j = 0; j < user.searchs[i].apparts.length; j++) {
  //                             appartList.push(user.searchs[i].apparts[j]);

  //                             k += 1;
  //                         }
  //                 }
  //                 appartList[0].like = Like.find({"appart":user.searchs[0].apparts[0]._id},{"flow":1});
  //                 // appartList[0].like = Like.find({"appart._id":"5866998238d53200047989e2"},{"flow":1});
  //                 //console.log(Like.find({"appart":ObjectId(user.searchs[0].apparts[0]._id)}).count());
  //                 app = user.searchs[0].apparts[0];
  //                 Like.find({"appart":user.searchs[0].apparts[0]},function(err, lll) {
  //                     if (err)
  //                         res.send(err);
  //                     console.log(lll);
  //                 });
  //                 res.json(appartList);
  //             });

  // on routes that end in /apparts/:appart_id
  // ----------------------------------------------------
  apiRouter.route('/apparts/:appart_id')

  // get the appart with that id (accessed at GET http://localhost:8080/api/apparts/:appart_id)
  .get(function(req, res) {
    Appart.findById(req.params.appart_id).populate('like').exec(function(err, appart) {
      if (err)
        res.send(err);
      res.json(appart);
    });
  })

  // update the appart with this id (accessed at PUT http://localhost:8080/api/apparts/:appart_id)
  .post(function(req, res) {

    // use our appart model to find the appart we want
    Appart.findById(req.params.appart_id, function(err, appart) {

      if (err)
        res.send(err);

      // save the appart
      appart.save(function(err) {
        if (err)
          res.send(err);

        res.json({
          message: 'Appart updated!'
        });
      });

    });
  })

  // delete the appart with this id (accessed at DELETE http://localhost:8080/api/apparts/:appart_id)
  .delete(function(req, res) {

    Appart.findById(req.params.appart_id, function(err, appart) {

      if (err)
        res.send(err);

      appart.remove(function(err) {
        if (err)
          res.send(err);

        res.json({
          message: 'Appart was successfully deleted'
        });
      });

    });

  });

  // ---------------------------------------------------------- LIKE ROUTES -------------------------------------


  apiRouter.route('/likes')

  .get(function(req, res) {
    Like.find({}).exec(function(err, likes) {
      if (err)
        res.send(err);

      res.send(likes); //58766640e5bc5d0004845b98 //58766653e5bc5d0004845b9d
    });

  });

  apiRouter.route('/likes/:like_id')

  .post(function(req, res) {
    Like.findById(req.params.like_id, function(err, like) {
      if (err)
        res.send(err);


      if (req.body.flow) {
        like.flow = req.body.flow;
      }

      if (req.body.note) {
        like.note = req.body.note;
      }

      like.save(function(err) {
        if (err)
          res.send(err);

        res.json({
          like
        });
      });

    });
  })

  .delete(function(req, res) {

    Like.findById(req.params.like_id, function(err, like) {

      if (err)
        res.send(err);

      like.remove(function(err) {
        if (err)
          res.send(err);

        res.json({
          message: 'Like was successfully deleted'
        });
      });

    });

  });

  // ----------------------------------------------- Add friend to search route ------------------------------------

  apiRouter.post('/invite-friend', function(req, res) {
    var referer = req.body.referer.name + " " + req.body.referer.surname;

    User.findOne({
      'username': req.body.username
    }, function(err, user) {
      if (!user) {
        async.waterfall([
          function(done) {
            crypto.randomBytes(20, function(err, buf) {
              var token = buf.toString('hex');
              done(err, token);
            });
          },
          function(token, done) {
            var expireDate = Date.now() + 3600000 * 24 * 7;
            user = new User({
              username: req.body.username,
              resetPasswordToken: token,
              resetPasswordExpires: expireDate
            });
            user.save(function(err) {
              done(err, token, user);
            });
          },
          function(token, user, done) {
            Search.findById(req.body.search, function(err, search) {
              search.users.push(user);
              search.save(function(err) {
                done(err, token, user);
              });
            });
          },
          function(token, user, done) {
            var transporter = nodemailer.createTransport({
              service: 'Gmail',
              auth: {
                user: 'cozyhome.contacts@gmail.com', // Your email id
                pass: 'stvznmqmfgcvlmoj' // Your password
              }
            });

            var mailOptions = {
              from: 'cozyhome.contacts@gmail.com', // sender address
              to: user.username,
              subject: 'Vous avez été invité à une recherche sur cozy-home', // Subject line
              text: 'Bonjour, vous avez été invité à une nouvelle recherche sur cozy-home par ' + referer + '. Vous pourrez la consulter sur www.cozy-home.fr une fois que vous aurez validé votre inscription. Pour terminer votre inscription, veuillez cliquer sur le lien suivant : ' + baseURL + '/auth/confirm-invite/' + token + '.' //, // plaintext body
                // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
            };

            transporter.sendMail(mailOptions, function(err, info) {
              req.flash('info', 'An e-mail has been sent to ' + user.username + ' with further instructions.');
              done(err, 'done');
            });

          }
        ], function(err) {
          if (err) return next(err);
          res.send('email envoyé');
          // res.send('/forgot');
        });
      } else {
        Search.findById(req.body.search, function(err, search) {
          if (err)
            res.send(err);
          if (search.users.map(String).indexOf(user._id.toString()) != -1) {
            res.json({
              error: "user already registered to the search"
            });
          } else {
            search.users.push(user);
            search.save(function(err) {
              if (err)
                res.send(err);

              var transporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                  user: 'cozyhome.contacts@gmail.com', // Your email id
                  pass: 'stvznmqmfgcvlmoj' // Your password
                }
              });

              var mailOptions = {
                from: 'cozyhome.contacts@gmail.com', // sender address
                to: user.username,
                subject: 'Vous avez été invité à une nouvelle recherche', // Subject line
                text: 'Bonjour, vous avez été invité à une nouvelle recherche par ' + referer + '. Vous pouvez la consulter sur www.cozy-home.fr'
                  // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
              };

              transporter.sendMail(mailOptions, function(error, info) {
                if (error) {
                  console.log(error);
                  res.json({
                    yo: 'error'
                  });
                } else {
                  console.log('Message sent: ' + info.response);
                  res.json({
                    yo: info.response
                  });
                }
              });

            });
          }
        });
      }
    });
  });

  return apiRouter;
};