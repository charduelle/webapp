var bodyParser = require('body-parser');
var passport = require('passport');
var User = require('../models/user');
var Search = require('../models/search');
var Token = require('../models/token');
var async = require('async');
var crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer');
var config = require('../config.js');

var baseURL = config.url();

module.exports = function(app, express) {

  // ROUTES FOR OUR API
  // =============================================================================
  var authRouter = express.Router(); // get an instance of the express Router

  // middleware to use for all requests
  authRouter.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
  });

  // End point for returning json data for the session user
  authRouter.get('/session', function(req, res) {
    res.send(req.isAuthenticated() ? req.user : '0');
  });

  authRouter.post('/register', function(req, res) {
    user = new User({
      username: req.body.username,
      name: req.body.name,
      surname: req.body.surname,
      admin: false
    });

    User.register(user,
      req.body.password,
      function(err, account) {
        if (err) {
          return res.status(500).json({
            err: err
          });
        }
        // passport.authenticate('local')(req, res, function () {
        //   return res.status(200).json({
        //     status: 'Registration successful!'
        //   });
        // });
        var authenticationURL = baseURL + '/auth/verify?authToken=' + account.authToken;
        var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'cozyhome.contacts@gmail.com', // Your email id
            pass: 'stvznmqmfgcvlmoj' // Your password
          }
        });

        var mailOptions = {
          from: 'cozyhome.contacts@gmail.com', // sender address
          to: account.username,
          subject: 'Dernière étape avant la création de votre compte', // Subject line
          html: '<a target=_blank href=\"' + authenticationURL + '\">Veuillez cliquer sur ce lien pour terminer votre inscription.</a>'
            // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
        };

        transporter.sendMail(mailOptions, function(err, info) {
          req.flash('info', 'An e-mail has been sent to ' + account.username + ' with further instructions.');
          res.status(200).json({
            status: 'Registration successful. Confirm email to activate the account!'
          });
        });
      });
  });

  authRouter.get('/verify', function(req, res) {
    User.verifyEmail(req.query.authToken, function(err, existingAuthToken) {
      if (err) console.log('err:', err);

      res.redirect(baseURL + '/landing/confirm_email');
    });
  });

  authRouter.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.status(401).json({
          err: info
        });
      }
      req.logIn(user, function(err) {
        if (err) {
          return res.status(500).json({
            err: 'Could not log in user'
          });
        }

        // issue a remember me cookie if the option was checked (pour l'instant, je le mets par défaut)
        if (!req.body.remember_me) {
          var token = new Token();

          token.user = user._id;

          token.save(function(err) {
            if (err)
              console.log(err);

            res.cookie('remember_me', token._id.toString(), {
              path: '/',
              httpOnly: true,
              maxAge: 6048000000
            }); // 7 days
            res.status(200).json(user);
          });
        } else {
          res.status(200).json(user);
        }

      });

    })(req, res, next);
  });

  authRouter.get('/logout', function(req, res) {
    res.clearCookie('remember_me');
    req.logout();
    res.status(200).json({
      status: 'Bye!'
    });
  });

  authRouter.post('/forgot', function(req, res, next) {
    async.waterfall([
      function(done) {
        crypto.randomBytes(20, function(err, buf) {
          var token = buf.toString('hex');
          done(err, token);
        });
      },
      function(token, done) {
        User.findOne({
          username: req.body.email
        }, function(err, user) {
          if (!user) {
            req.flash('error', 'No account with that email address exists.');
            return res.redirect('/forgot');
          }

          user.resetPasswordToken = token;
          user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
          user.save(function(err) {
            done(err, token, user);
          });
        });
      },
      function(token, user, done) {
        var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'cozyhome.contacts@gmail.com', // Your email id
            pass: 'stvznmqmfgcvlmoj' // Your password
          }
        });

        var mailOptions = {
          from: 'cozyhome.contacts@gmail.com', // sender address
          to: user.username,
          subject: 'Réinitialisation de mot de passe', // Subject line
          //text: baseURL + '/auth/reset/' + token //, // plaintext body
          html: '<a target=_blank href=\"' + baseURL + '/auth/reset/' + token + '\">Veuillez cliquer sur ce lien pour réinitialiser votre mot de passe.</a>'
        };

        transporter.sendMail(mailOptions, function(err, info) {
          req.flash('info', 'An e-mail has been sent to ' + user.username + ' with further instructions.');
          done(err, 'done');
        });

      }
    ], function(err) {
      if (err) return next(err);
      res.send('email envoyé');
      // res.send('/forgot');
    });
  });

  authRouter.route('/reset/:token')

  .get(function(req, res) {
    User.findOne({
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
        $gt: Date.now()
      }
    }, function(err, user) {
      if (!user) {
        req.flash('error', 'Password reset token is invalid or has expired.');
        return res.redirect('/');
      }
      return res.redirect(baseURL + '/landing/reset_password/' + req.params.token);
    });
  })

  .post(function(req, res) {
    async.waterfall([
      function(done) {
        User.findOne({
          resetPasswordToken: req.params.token,
          resetPasswordExpires: {
            $gt: Date.now()
          }
        }, function(err, user) {
          if (!user) {
            req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect('back');
          }

          user.setPassword(req.body.password, function() {

            //password = req.body.password;
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;

            user.save(function(err) {

              req.logIn(user, function(err) {
                done(err, user);
              });
            });
          });
        });
      },
      function(user, done) {

        var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'cozyhome.contacts@gmail.com', // Your email id
            pass: 'stvznmqmfgcvlmoj' // Your password
          }
        });

        var mailOptions = {
          from: 'cozyhome.contacts@gmail.com', // sender address
          to: user.username,
          subject: 'Confirmation de modification de mot de passe', // Subject line
          text: 'Votre mot de passe a bien modifié.' // plaintext body
            // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
        };

        transporter.sendMail(mailOptions, function(err, info) {
          req.flash('success', 'Success! Your password has been changed.');
          done(err, 'done');
        });

      }
    ], function(err) {
      res.redirect('/');
    });
  });

  authRouter.route('/confirm-invite/:token')

  .get(function(req, res) {
    User.findOne({
      resetPasswordToken: req.params.token,
      resetPasswordExpires: {
        $gt: Date.now()
      }
    }, function(err, user) {
      if (!user) {
        req.flash('error', 'Password reset token is invalid or has expired.');
        return res.redirect('/');
      }
      return res.redirect(baseURL + '/landing/confirm_invite/' + req.params.token);
    });
  })

  .post(function(req, res) {
    async.waterfall([
      function(done) {
        User.findOne({
          resetPasswordToken: req.params.token,
          resetPasswordExpires: {
            $gt: Date.now()
          }
        }, function(err, user) {
          if (!user) {
            req.flash('error', 'Password reset token is invalid or has expired.');
            return res.redirect('back');
          }

          user.setPassword(req.body.password, function() {

            //password = req.body.password;
            user.resetPasswordToken = undefined;
            user.resetPasswordExpires = undefined;
            user.name = req.body.name;
            user.surname = req.body.surname;

            user.save(function(err) {

              req.logIn(user, function(err) {
                done(err, user);
              });
            });
          });
        });
      },
      function(user, done) {

        var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'cozyhome.contacts@gmail.com', // Your email id
            pass: 'stvznmqmfgcvlmoj' // Your password
          }
        });

        var mailOptions = {
          from: 'cozyhome.contacts@gmail.com', // sender address
          to: user.username,
          subject: 'Confirmation de votre inscription sur cozy-home.fr', // Subject line
          text: 'Votre inscription sur cozy-home est maintenant terminée. Bonne recherche !' // plaintext body
            // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
        };

        transporter.sendMail(mailOptions, function(err, info) {
          req.flash('success', 'Success! Your subscription is OK.');
          done(err, 'done');
        });

      }
    ], function(err) {
      res.redirect('/');
    });
  });

  return authRouter;
};