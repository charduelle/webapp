var bodyParser = require('body-parser');
var passport = require('passport');
var Appart = require('../models/appart');
var User = require('../models/user');
var Search = require('../models/search');
var Like = require('../models/like');
var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var config = require('../config.js');

var baseURL = config.url();

module.exports = function(app, express) {

  // ROUTES FOR OUR API
  // =============================================================================
  var clientRouter = express.Router(); // get an instance of the express Router

  // ---------------------------------------------------------- LIKE ROUTES -------------------------------------

  clientRouter.route('/likes/:like_id/:user_id/like')

  .get(function(req, res) {
    Like.findById(req.params.like_id, function(err, like) {

      if (err)
        res.send("Oups, il semblerait que l'annonce ait été supprimée.");

      if (like) {
        for (var i = 0; i < like.users.length; i++) {
          if (req.params.user_id == like.users[i]._id) {
            like.users[i].status = "like";
            like.markModified('users');
          }
        }

        like.flow = "Ajouté aux favoris";

        like.save(function(err) {
          if (err)
            console.log(err);
        });

        Appart.findById(like.appart, function(err, appart) {

          if (err)
            console.log(err);

          Search.findById(like.search, function(err, search) {

            if (err)
              console.log(err);

            appart.like = like;

            appart.search = search;

            appart.save(function(err) {
              if (err)
                console.log(err);
            });

          });

        });

        // function checkLike(users) {
        // 	return users['status'] == "like";
        // }

        //    if (like.users.every(checkLike)) {

        // }

        //res.send("Thank you!");
        res.redirect(baseURL + '/landing/like_appart');
      }

    });

  });

  clientRouter.route('/searchs/:search_id')

  .get(function(req, res) {
    Search.findById(req.params.search_id, function(err, search) {

      if (err)
        res.send(err);

      search.status = false;

      search.save(function(err) {

        if (err)
          res.send(err);
      });

      res.redirect(baseURL + '/landing/pause_search');

    });

  });

  clientRouter.route('/mailer')

  .post(function(req, res) {
    var transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: 'cozyhome.contacts@gmail.com', // Your email id
        pass: 'stvznmqmfgcvlmoj' // Your password
      }
    });

    var text = 'Bonjour, \n\n';
    text += 'Vous avez reçu un nouvel email de la part de ' + req.body.name + '\n';
    text += 'Numéro de téléphone : ' + req.body.phone + '\n';
    text += 'Email : ' + req.body.email + '\n';
    text += 'Message : ' + req.body.message + '\n';

    var mailOptions = {
      from: 'cozyhome.contacts@gmail.com', // sender address
      to: ['robin.bonduelle@gmail.com', 'gwenn.charlot@gmail.com'], // list of receivers
      subject: 'Nouveau contact', // Subject line
      text: text //, // plaintext body
        // html: '<b>Hello world ✔</b>' // You can choose to send an HTML body instead
    };

    transporter.sendMail(mailOptions, function(error, info) {
      if (error) {
        console.log(error);
        res.json({
          yo: 'error'
        });
      } else {
        console.log('Message sent: ' + info.response);
        res.json({
          yo: info.response
        });
      }
    });

  });

  // apiRouter.route('/searchs/:search_id/:user_id/:appart_id')

  // .get(function(req, res) {
  //     Appart.findById(req.params.appart_id, function(err, appart) {

  //         if (err)
  //             res.send(err);

  //         Search.findById(req.params.search_id, function(err, search) {

  //             if (err)
  //                 res.send(err);                                            

  //             appart.searchLikes.push(search._id);

  //             if (appart.searchLikes.length = search.users.length) {

  //                 search.apparts.push(appart._id);  // update the apparts info

  //                 search.save(function(err) {
  //                     if (err)
  //                         res.send(err);

  //                     res.json({ message: 'Appart was added to the search!' });
  //                     //res.json({search});
  //                 });
  //             }

  //             //appart.search = search
  //             appart.save(function(err) {
  //                 if (err)
  //                         res.send(err);
  //             });

  //         });

  //     });
  // });

  return clientRouter;
};