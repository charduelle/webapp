var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var User = require('./user.js');
var Appart = require('./appart.js');
var Like = require('./like.js');
var relationship = require('mongoose-relationship');

var SearchSchema = new Schema({

  created_at: Date,
  updated_at: Date,

  name: String,
  idtt: Number,
  idtypebien: Number,
  city: String,
  ci: Array,
  pxmin: Number,
  pxmax: Number,
  surfacemin: Number,
  surfacemax: Number,
  nb_piecesmin: Number,
  nb_piecesmax: Number,
  nb_chambresmin: Number,

  status: {
    type: Boolean,
    default: true
  },
  users: [{
    type: Schema.ObjectId,
    ref: 'User',
    childPath: 'searchs'
  }],
  apparts: [{
    type: Schema.ObjectId,
    ref: 'Appart'
  }]
});

// on every save, add the date
SearchSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

SearchSchema.pre('remove', function(next) {

  this.model('User').update({
      _id: {
        $in: this.users
      }
    }, {
      $pull: {
        searchs: this._id
      }
    }, {
      multi: true
    },
    next
  );

  this.model('Appart').update({
      _id: {
        $in: this.apparts
      }
    }, {
      $pull: {
        search: this._id
      }
    }, {
      multi: true
    },
    next
  );
});

SearchSchema.pre('remove', function(next) {
  Like.remove({
    search: this._id
  }, next);
});

SearchSchema.plugin(relationship, {
  relationshipPathName: 'users'
});


module.exports = mongoose.model('Search', SearchSchema);