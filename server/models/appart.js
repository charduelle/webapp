var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Search = require('./search.js');
var Like = require('./like.js');
var relationship = require("mongoose-relationship");

var AppartSchema = new Schema({
  created_at: Date,
  updated_at: Date,
  batch: Number,

  nbPictures: Number,
  price: Number,
  parking: Number,
  title: String,
  transactionType: Number,
  link: String,
  arrondissement: Number,
  country: String,
  type: Number,
  pm: Number,
  description: String,
  creationDate: Date,
  box: Number,
  surface: Number,
  rooms: Number,
  label: String,
  mainId: String,
  id: String,
  bigPhotos: Array,
  stdPhotos: Array,
  thbPhotos: Array,
  metro: String,
  bedrooms: Number,
  terrasse: Number,
  city: String,

  search: [{
    type: Schema.ObjectId,
    ref: 'Search',
    childPath: 'apparts'
  }],
  like: [{
    type: Schema.ObjectId,
    ref: 'Like'
  }]
});

// on every save, add the date
AppartSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});


AppartSchema.pre('remove', function(next) {

  this.model('Search').update({
      _id: {
        $in: this.search
      }
    }, {
      $pull: {
        apparts: this._id
      }
    }, {
      multi: true
    },
    next
  );

});

AppartSchema.pre('remove', function(next) {
  Like.remove({
    appart: this._id
  }, next);
});

AppartSchema.plugin(relationship, {
  relationshipPathName: 'search'
});

module.exports = mongoose.model('Appart', AppartSchema);