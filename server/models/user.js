var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');
var passportLocalMongooseEmail = require('passport-local-mongoose-email');
var Search = require('./search.js');

var UserSchema = new Schema({
  created_at: Date,
  updated_at: Date,
  name: String,
  surname: String,
  username: String, //{type: String, required: true, unique: true},
  admin: Boolean,
  password: String,
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  searchs: [{
    type: Schema.ObjectId,
    ref: 'Search'
  }]
});

// on every save, add the date
UserSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

// UserSchema.pre('remove', function(next) {
//     
// });

UserSchema.pre('save', function(next) {

  this.searchs = this.searchs.filter(function(item, pos, self) {
    return self.indexOf(item) == pos;
  });

  next();

});

UserSchema.pre('remove', function(next) {

  this.model('Search').update({
      _id: {
        $in: this.searchs
      }
    }, {
      $pull: {
        users: this._id
      }
    }, {
      multi: true
    },
    next
  );

});

UserSchema.pre('remove', function(next) {

  this.model('Search').remove({
    users: []
  }, next);

});

//cutom method example
// UserSchema.methods.dudify = function() {
//   // add some stuff to the users name
//   this.name = this.name + '-dude'; 

//   return this.name;
// };

UserSchema.plugin(passportLocalMongoose);
UserSchema.plugin(passportLocalMongooseEmail);

module.exports = mongoose.model('User', UserSchema);