var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Appart = require('./appart.js');
var Search = require('./search.js');
var relationship = require("mongoose-relationship");


var LikeSchema = new Schema({
  created_at: Date,
  updated_at: Date,
  flow: String,
  users: Array,
  note: String,

  search: [{
    type: Schema.ObjectId,
    ref: 'Search'
  }],
  appart: [{
    type: Schema.ObjectId,
    ref: 'Appart',
    childPath: 'like'
  }]
});

// on every save, add the date
LikeSchema.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});

LikeSchema.pre('remove', function(next) {

  this.model('Appart').update({
      _id: {
        $in: this.appart
      }
    }, {
      $pull: {
        like: this._id
      }
    }, {
      multi: true
    },
    next
  );

});

LikeSchema.pre('remove', function(next) {

  this.model('Search').update({
      _id: {
        $in: this.search
      }
    }, {
      $pull: {
        apparts: this.appart[0]
      }
    }, {
      multi: true
    },
    next
  );

});

LikeSchema.plugin(relationship, {
  relationshipPathName: 'appart'
});

module.exports = mongoose.model('Like', LikeSchema);