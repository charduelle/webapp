var gulp = require('gulp');
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');
var sass = require('gulp-sass') ;
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var minifycss = require('gulp-minify-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var size = require('gulp-filesize');
var notify = require("gulp-notify") 
var bower = require('gulp-bower');

gulp.task('default', ['browser-sync','styles'], function () {
	gulp.watch(['client/assets/css/**/*.scss'], ['styles']);
});

gulp.task('browser-sync', ['nodemon'], function() {
	browserSync.init(null, {
		proxy: "http://localhost:8080",
        files: ["client/**/*.*"],
        browser: "google chrome",
        port: 7000,
	});
});

gulp.task('nodemon', function (cb) {
	
	var started = false;
	
	return nodemon({
		script: 'server.js'
	}).on('start', function () {
		// to avoid nodemon being started multiple times
		if (!started) {
			cb();
			started = true; 
		} 
	});
});

gulp.task('bower', function() { 
    return bower()
         .pipe(gulp.dest('./bower_components/')) 
});

gulp.task('styles', function () {
    return gulp.src('./client/assets/css/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 2 version'))
    .pipe(rename('main.css'))
    .pipe(minifycss())
    .pipe(gulp.dest('./client/assets/css'))
    .pipe(size())
    .on('end', function(){
        gutil.log(gutil.colors.yellow('♠ La tâche CSS est terminée.'));
    });
});