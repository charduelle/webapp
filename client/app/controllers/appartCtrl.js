angular.module('appartCtrl', ['appartService'])

.controller('appartController', function($scope, Appart, Like, growl, $timeout, $cookieStore) {

	// set a processing variable to show loading things
	$scope.processing = true;

	if ($cookieStore.get('filters')) {
		$scope.filters = $cookieStore.get('filters');
	} else {
		$scope.filters = {};
		$scope.filters.like = [];
		$scope.filters.search = [];
	}

	$scope.orderOptions = {
		"created_at": "nouveautés",
		"surface": "surface",
		"price": "prix",
		"arrondissement": "arrondissement"
	};

	$scope.orderLabel = "nouveautés";
	$scope.order = "created_at";
	// because we filter on nouveautés, we need to reverse the order to have latest apparts first
	$scope.reverse = "true";

	// grab all the apparts at page load
	$scope.getApparts = function() {
		Appart.all()
			.success(function(data) {

				$scope.searchs = data.searchs;

				$scope.apparts = [];

				$scope.apparts = $scope.apparts.concat($.map($scope.searchs, function(elem) {
					return elem.apparts;
				}));

				$scope.minSurface = Math.min.apply(Math, $scope.apparts.map(function(o) {
					return o.surface;
				}));
				$scope.maxSurface = Math.max.apply(Math, $scope.apparts.map(function(o) {
					return o.surface;
				}));

				$scope.surfaceSlider = {
					minValue: $scope.minSurface,
					maxValue: $scope.maxSurface,
					options: {
						floor: $scope.minSurface,
						ceil: $scope.maxSurface,
						step: 1,
						translate: function(value) {
							return value + ' m2';
						}
					}
				};

				$scope.minPrice = Math.min.apply(Math, $scope.apparts.map(function(o) {
					return o.price;
				}));
				$scope.maxPrice = Math.max.apply(Math, $scope.apparts.map(function(o) {
					return o.price;
				}));

				$scope.priceSlider = {
					minValue: $scope.minPrice,
					maxValue: $scope.maxPrice,
					options: {
						floor: $scope.minPrice,
						ceil: $scope.maxPrice,
						step: 1,
						translate: function(value) {
							return value + ' €';
						}
					}
				};

				$scope.processing = false;

				// to force the sliders to render again when appart values are returned
				$timeout(function() {
					$scope.$broadcast('rzSliderForceRender');
				});

			});
	};

	// $scope.getAppart() = function(id) {
	// 	Appart.get($routeParams.appart_id)
	// 	.success(function (data) {
	// 		// when all the apparts come back, remove the processing variable
	// 		$scope.appart = data;
	// 	});
	// };

	$scope.getApparts();

	$scope.flows = ["Ajouté aux favoris", "Informations demandées", "Visite demandée", "Offre envoyée", "Offre acceptée"];

	$scope.updateLikeFlow = function(id, flow, appart) {

		Like.update(id, {
			"flow": flow
		}).success(function(data) {
			appart.like[0].flow = flow;
		});
	};

	$scope.deleteLike = function(id) {
		Like.delete(id).success(function(data) {
			$scope.getApparts();
			growl.success("L'appartement a bien été supprimé de vos favoris.");
		});
	};

	$scope.surfaceRange = function(item) {
		return (parseInt(item.surface) >= $scope.surfaceSlider.minValue && parseInt(item.surface) <= $scope.surfaceSlider.maxValue);
	};

	$scope.priceRange = function(item) {
		return (parseInt(item.price) >= $scope.priceSlider.minValue && parseInt(item.price) <= $scope.priceSlider.maxValue);
	};

	$scope.setFilter = function(filter, key) {
		if (filter == "like") {
			$scope.filters[filter][0] = {
				flow: key
			};
		} else if (filter == "arrondissement") {
			$scope.filters[filter] = key;
		} else if (filter == "search") {
			$scope.filters[filter][0] = key;
		}
		$cookieStore.put('filters', $scope.filters);
	};

	$scope.resetFilters = function() {
		$cookieStore.remove("filters");
		$scope.filters = {};
		$scope.filters.like = [];
		$scope.filters.search = [];

		$scope.surfaceSlider = {
			minValue: $scope.minSurface,
			maxValue: $scope.maxSurface,
			options: {
				floor: $scope.minSurface,
				ceil: $scope.maxSurface,
				step: 1,
				translate: function(value) {
					return value + ' m2';
				}
			}
		};

		$scope.priceSlider = {
			minValue: $scope.minPrice,
			maxValue: $scope.maxPrice,
			options: {
				floor: $scope.minPrice,
				ceil: $scope.maxPrice,
				step: 1,
				translate: function(value) {
					return value + ' €';
				}
			}
		};

	};

	$scope.activeFilters = function() {
		return JSON.stringify($scope.filters) ==  JSON.stringify({"like":[],"search":[]});
	};

	$scope.updateOrder = function(key, data) {
		$scope.reverse = (key === 'created_at');
		$scope.order = key;
		$scope.orderLabel = data;
	};

	// $('.filterMenu').affix({
	//      	offset: {
	//        top: 90
	//      }
	// });

	// function leftBarControl(){
	//     var windowHeight = $(window).height();
	//     var scrollHeight = $(window).scrollTop();
	//     var leftBarWidth = $('.main').width()*.25 //20% of .main width
	//     var leftBarHeight = $('.filterMenu').outerHeight();
	//     var leftBarTop = 90; //30 because .head is 30px high
	//     if(windowHeight - 90 < leftBarHeight){ //Again including 30 because of .head
	//         leftBarTop = windowHeight - leftBarHeight;
	//     }
	//     if((windowHeight + scrollHeight) >= leftBarHeight){
	//         $('.filterMenu').css({
	//             position:'fixed',
	//             top: leftBarTop,
	//             width: leftBarWidth
	//         })
	//     }
	//     else{
	//          $('.filterMenu').css({
	//             position: 'static',
	//             left: '',
	//             top: '',
	//             width: '20%'
	//         })
	//     }
	// }
	// $(window).scroll(leftBarControl); //Run control on window scroll
	// $(window).resize(leftBarControl); //Run control on window resize

})

.controller('appartShowController', function($scope, $routeParams, Appart, Like, growl, $location) {

	// set a processing variable to show loading things
	$scope.processing = true;
	$scope.flows = ["Ajouté aux favoris", "Informations demandées", "Visite demandée", "Offre envoyée", "Offre acceptée"];

	$scope.updateLikeFlow = function(id, flow, appart) {
		Like.update(id, {
			"flow": flow
		}).success(function(data) {
			appart.like[0].flow = flow;
		});
	};

	$scope.deleteLike = function(id) {
		Like.delete(id).success(function(data) {
			$location.path('/apparts');
			growl.success("L'appartement a bien été supprimé de vos favoris.");
		});
	};

	$scope.updateLikeNote = function(id, note, appart) {
		Like.update(id, {
			"note": note
		}).success(function(data) {
			growl.success("La note a bien été mise à jour.");
      $('#addNoteModal').modal('hide');
		});
	};

	// get the appart data for the appart you want to edit
	// $routeParams is the way we grab data from the URL
	Appart.get($routeParams.appart_id)
		.success(function(data) {
			// when all the apparts come back, remove the processing variable
			$scope.processing = false;
			$scope.appart = data;
			$scope.picturesLength = data.stdPhotos.length;
		});

	$scope.pictureDetails = function(index) {
		$scope.bigPicAvailable = $scope.appart.bigPhotos[index] ? true : false;
		$scope.currentPicture = $scope.bigPicAvailable ? $scope.appart.bigPhotos[index] : $scope.appart.stdPhotos[index];
		$scope.carouselRight = index + 1 > $scope.picturesLength - 1 ? 0 : index + 1;
		$scope.carouselLeft = index - 1 < 0 ? $scope.picturesLength - 1 : index - 1;
		jQuery('#picture-modal').modal();
	};

})

// controller applied to appart edit page
.controller('appartEditController', function($routeParams, Appart) {

	var vm = this;

	// variable to hide/show elements of the view
	// differentiates between create or edit pages
	vm.type = 'edit';

	// get the appart data for the appart you want to edit
	// $routeParams is the way we grab data from the URL
	Appart.get($routeParams.appart_id)
		.success(function(data) {
			vm.appartData = data;
		});

	// function to save the appart
	vm.saveAppart = function() {
		vm.processing = true;
		vm.message = '';

		// call the appartService function to update 
		User.update($routeParams.appart_id, vm.appartData)
			.success(function(data) {
				vm.processing = false;

				// clear the form
				vm.appartData = {};

				// bind the message from our API to vm.message
				vm.message = data.message;
			});
	};

});