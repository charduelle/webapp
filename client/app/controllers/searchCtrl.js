angular.module('searchCtrl', ['searchService'])

.controller('searchController', function(Search, $scope, growl, $location) {

  // set a processing variable to show loading things
  $scope.processing = true;

  $scope.getSearchs = function() {
    Search.all().success(function(data) {
      $scope.processing = false;
      $scope.searchs = data;
    });
  };

  // grab all the searchs at page load
  $scope.getSearchs();


  $scope.deleteSearch = function(id) {

    Search.delete(id).success(function(data) {
      $scope.getSearchs();
      growl.success('Votre recherche a bien été supprimée.');
    });
  };

  $scope.toggleStatusSearch = function(id) {
    Search.toggleStatus(id).success(function(data) {
      $scope.getSearchs();
      growl.success('Votre recherche a bien été mise à jour.');
    });
  };

})

.controller('searchShowController', function($routeParams, $location, Search, Auth, $scope, growl) {

  // set a processing variable to show loading things
  $scope.processing = true;
  $scope.friendForm = {};
  Auth.userData().success(function(data) {
    $scope.friendForm.referer = data;
  });
  // $scope.friendForm.referer = "Robin";
  $scope.form = {};

  $scope.getSearch = function() {
    Search.get($routeParams.search_id).success(function(data) {
      $scope.processing = false;
      $scope.search = data;
      $scope.friendForm.search = $scope.search._id;
    });
  };

  $scope.getSearch();

  $scope.toggleStatusSearch = function(id) {
    Search.toggleStatus(id).success(function(data) {
      $scope.getSearch();
      growl.success('Votre recherche a bien été mise à jour.');
    });
  };

  $scope.deleteSearch = function(id) {
    Search.delete(id).success(function(data) {
      $location.path('/searchs');
      growl.success('Votre recherche a bien été supprimée.');
    });
  };

  $scope.inviteFriend = function() {
    Search.inviteFriend($scope.friendForm)
      .success(function(data) {

        growl.success($scope.friendForm.username + ' a été ajouté à la recherche.');
        $('#inviteFriendModal').modal('hide');
        $scope.friendForm = {};
        $scope.form.invite.$setPristine();
      });
  };

})

.controller('searchCreateController', function(Search, Auth, $location, $scope, growl) {

  $scope.searchData = {};
  $scope.searchData.idtt = 2;
  $scope.searchData.idtypebien = 1;
  $scope.searchData.city = "Paris";
  Auth.userData().success(function(data) {
    $scope.searchData.referer = data;
  });


  $scope.arr = {};
  $scope.arrs = [{
    label: '75001',
    id: 75001
  }, {
    label: '75002',
    id: 75002
  }, {
    label: '75003',
    id: 75003
  }, {
    label: '75004',
    id: 75004
  }, {
    label: '75005',
    id: 75005
  }, {
    label: '75006',
    id: 75006
  }, {
    label: '75007',
    id: 75007
  }, {
    label: '75008',
    id: 75008
  }, {
    label: '75009',
    id: 75009
  }, {
    label: '75010',
    id: 75010
  }, {
    label: '75011',
    id: 75011
  }, {
    label: '75012',
    id: 75012
  }, {
    label: '75013',
    id: 75013
  }, {
    label: '75014',
    id: 75014
  }, {
    label: '75015',
    id: 75015
  }, {
    label: '75016',
    id: 75016
  }, {
    label: '75017',
    id: 75017
  }, {
    label: '75018',
    id: 75018
  }, {
    label: '75019',
    id: 75019
  }, {
    label: '75020',
    id: 75020
  }];

  // function to create a search
  $scope.saveSearch = function() {

    if ($scope.searchData.city == "Paris") {
      $scope.searchData.ci = $scope.arr.selected.map(function(el) {
        return el.id;
      });
    } else if ($scope.searchData.city == "Bordeaux") {
      $scope.searchData.ci = [33000];
    } else if ($scope.searchData.city == "Lyon") {
      $scope.searchData.ci = [69001, 69002, 69003, 69004, 69005, 69006, 69007];
    }

    if ($scope.searchData.users) {
      $scope.searchData.users = $scope.searchData.users.map(function(el) {
        return el.text;
      });
    }
    // use the create function in the searchService
    Search.create($scope.searchData)
      .success(function(data) {
        $scope.searchData = {};
        $location.path('/searchs');
        growl.success('Votre recherche a bien été créée.');
        $scope.form.$setPristine();
      });
  };

})

.controller('searchEditController', function(Search, Auth, $routeParams, $location, $scope, growl) {

  Search.get($routeParams.search_id).success(function(data) {
      $scope.searchData = data; 
  });

  $scope.arr = {};

  $scope.arrs = [{
    label: '75001',
    id: 75001
  }, {
    label: '75002',
    id: 75002
  }, {
    label: '75003',
    id: 75003
  }, {
    label: '75004',
    id: 75004
  }, {
    label: '75005',
    id: 75005
  }, {
    label: '75006',
    id: 75006
  }, {
    label: '75007',
    id: 75007
  }, {
    label: '75008',
    id: 75008
  }, {
    label: '75009',
    id: 75009
  }, {
    label: '75010',
    id: 75010
  }, {
    label: '75011',
    id: 75011
  }, {
    label: '75012',
    id: 75012
  }, {
    label: '75013',
    id: 75013
  }, {
    label: '75014',
    id: 75014
  }, {
    label: '75015',
    id: 75015
  }, {
    label: '75016',
    id: 75016
  }, {
    label: '75017',
    id: 75017
  }, {
    label: '75018',
    id: 75018
  }, {
    label: '75019',
    id: 75019
  }, {
    label: '75020',
    id: 75020
  }];

  // function to create a search
  $scope.editSearch = function() {

    if ($scope.searchData.city == "Paris") {
      $scope.searchData.ci = $scope.arr.selected.map(function(el) {
        return el.id;
      });
    } else if ($scope.searchData.city == "Bordeaux") {
      $scope.searchData.ci = [33000];
    } else if ($scope.searchData.city == "Lyon") {
      $scope.searchData.ci = [69001, 69002, 69003, 69004, 69005, 69006, 69007];
    }

    // use the create function in the searchService
    Search.update($routeParams.search_id, $scope.searchData)
      .success(function(data) {
        $scope.searchData = {};
        $location.path('/searchs');
        growl.success('Votre recherche a bien été modifiée.');
        $scope.form.$setPristine();
      });
  };

});