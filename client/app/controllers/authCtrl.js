angular.module('authCtrl', ['authService'])

.controller('loginController', function (Auth, $location, $rootScope, $scope) {

  $scope.loginForm = {};
  
  $scope.loginUser = function () {

    // initial values
    $scope.error = false;
    $scope.disabled = true;

    // call login from service
    Auth.login($scope.loginForm.username, $scope.loginForm.password)
      // handle success
      .then(function () {
        $rootScope.$broadcast('log');
        $('#loginModal').modal('hide');
        $location.path('/apparts');
        $scope.disabled = false;
        $scope.loginForm = {};
      })
      // handle error
      .catch(function () {
        $scope.error = true;
        $scope.errorMessage = "Email ou mot de passe incorrect.";
        $scope.disabled = false;
        $scope.loginForm = {};
      });
  }

   

})

.controller('logoutController', function (Auth, $location, $rootScope, $scope) {

  $scope.logoutUser = function () {

    // call logout from service
    Auth.logout()
        .then(function () {
          $rootScope.$broadcast('log');
          $location.path('/');
        });

  }

})

.controller('registerController', function (Auth, $location, $rootScope, $scope, growl) {

  $scope.registerForm = {};

  $scope.registerUser = function () {

   // initial values
    $scope.error = false;
    $scope.disabled = true;

    // call register from service
    Auth.register($scope.registerForm)
      // handle success
      .then(function () {
        $('#registerModal').modal('hide');
        $scope.disabled = false;
        $scope.registerForm = {};
        growl.success("Un email vient d'être envoyé à votre adresse. Cliquez sur le lien qu'il contient pour terminer votre inscription.");
        // Auth.login($scope.registerForm.username, $scope.registerForm.password)
        //   .then(function () {
        //     $rootScope.$broadcast('log');
        //     $location.path('/apparts');
        //     $('#registerModal').modal('hide');
        //     $scope.disabled = false;
        //     $scope.registerForm = {};
        //     growl.success("Votre compte a bien été créé.");
        //   });
      })
      // handle error
      .catch(function (err) {
        $scope.error = true;
        if (err.err.name == "UserExistsError") {
          $scope.errorMessage = "Cet email existe déjà, veuillez en choisir un autre.";
        } else {
          $scope.errorMessage = "Une erreur est survenue, veuillez réessayer.";
        }
        $scope.disabled = false;
        $scope.registerForm = {};
      });
  }

});

