angular.module('layoutCtrl', ['authService'])

.controller('headerController', function(Auth, $rootScope, $scope, $location) {

  Auth.userData().success(function(data) {
    $scope.user = data;
  });

  $rootScope.$on('log', function(event) {
    Auth.userData().success(function(data) {
      $scope.user = data;
    });
  });

  $scope.isActive = function (viewLocation) { 
    if (viewLocation == "/") {
      return viewLocation === $location.path();
    } else {
      return $location.path().indexOf(viewLocation) > -1;
    }
    // viewLocation === $location.path();
  };
  
});