angular.module('userCtrl', ['userService'])

.controller('userController', function ($scope, User, growl) {

	// set a processing variable to show loading things
	$scope.processing = true;

	// grab all the apparts at page load
	$scope.getUsers = function() {
		User.all()
		.success(function (data) {			
			$scope.users = data;
		    $scope.processing = false;
		});
	};

	$scope.getUsers();

	$scope.deleteUser = function(id) {
		User.delete(id)
		.success(function () {			
			$scope.getUsers();
		    growl.success("L'utilisateur a bien été supprimé.");
		});
	};

})

.controller('userShowController', function ($scope, User, $routeParams, growl) {

	// set a processing variable to show loading things
	$scope.processing = true;
	$scope.userData =  {};

	// get the user data for the user you want to edit
	// $routeParams is the way we grab data from the URL
	User.get($routeParams.user_id)
		.success(function (data) {
			// when all the users come back, remove the processing variable
			$scope.processing = false;
			$scope.user = data;
			$scope.userData.email = data.username;
		});

	$scope.forgotPassword = function() {
		User.forgotPassword($scope.userData)
		.success(function () {
			growl.success("Un email vient de vous être envoyé. Suivez le lien qu'il contient pour modifier votre mot de passe.");
		});
	};

})

.controller('userUpdateController', function ($scope, User, $routeParams, $location, growl) {

	$scope.userData =  {};
	$scope.userData.password = "";
	$scope.userData.name = "";
	$scope.userData.surname = "";

	$scope.resetPassword = function() {
		User.resetPassword($routeParams.token, $scope.userData)
		.success(function () {
			$location.path('/apparts');
			growl.success("Votre mot de passe vient d'être modifié.");
			$scope.userData =  {};
		});
	};

	$scope.confirmInvite = function() {
		User.confirmInvite($routeParams.token, $scope.userData)
		.success(function () {
			$location.path('/apparts');
			growl.success("Votre inscription est maintenant terminée. Bonne recherche !");
			$scope.userData =  {};
		});
	};

});

