angular.module('userService', [])

.factory('User', function($http) {

	// create a new object
	var userFactory = {};

	// get a single appart
	userFactory.get = function(id) {
		return $http.get('/api/users/' + id);
	};

	// get all users
	userFactory.all = function() {
		return $http.get('/api/users/');
	};

	// forgot password
	userFactory.forgotPassword = function(userData) {
		return $http.post('/auth/forgot', userData);
	};

	// reset password
	userFactory.resetPassword = function(token, userData) {
		return $http.post('/auth/reset/' + token, userData);
	};

	// reset password
	userFactory.confirmInvite = function(token, userData) {
		return $http.post('/auth/confirm-invite/' + token, userData);
	};

	// delete a user
	userFactory.delete = function(id) {
		return $http.delete('/api/users/' + id);
	};
	
	// // create a user
	// userFactory.create = function(userData) {
	// 	return $http.post('/api/users/', userData);
	// };

	// // update a user
	// userFactory.update = function(id, userData) {
	// 	return $http.put('/api/users/' + id, userData);
	// };

	

	// return our entire userFactory object
	return userFactory;

});