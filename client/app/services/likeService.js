angular.module('likeService', [])

.factory('Like', function($http) {

	// create a new object
	var likeFactory = {};

	// get all apparts
	likeFactory.update = function(id, likeData) {
		return $http.post('/api/likes/' + id, likeData);
	};

	likeFactory.delete = function(id) {
		return $http.delete('/api/likes/' + id);
	};
	
	// return our entire likeFactory object
	return likeFactory;

});