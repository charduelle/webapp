angular.module('appartService', [])

.factory('Appart', function($http) {

	// create a new object
	var appartFactory = {};

	// get a single appart
	appartFactory.get = function(id) {
		return $http.get('/api/apparts/' + id);
	};

	// get all apparts
	appartFactory.all = function() {
		return $http.get('/api/apparts/');
	};

	// update an appart
	appartFactory.update = function(id, appartData) {
		return $http.post('/api/apparts/' + id, appartData);
	};
	
	// // create a user
	// userFactory.create = function(userData) {
	// 	return $http.post('/api/users/', userData);
	// };

	// // delete a user
	// userFactory.delete = function(id) {
	// 	return $http.delete('/api/users/' + id);
	// };

	// return our entire userFactory object
	return appartFactory;

});