angular.module('searchService', [])

.factory('Search', function($http) {

	var searchFactory = {};

	searchFactory.get = function(id) {
		return $http.get('/api/searchs/' + id);
	};

	searchFactory.all = function() {
		return $http.get('/api/searchs');
	};

	searchFactory.create = function(searchData) {
		return $http.post('api/searchs', searchData);
	};

	searchFactory.update = function(id, searchData) {
		return $http.post('/api/searchs/' + id, searchData);
	};

	searchFactory.toggleStatus = function(id) {
		return $http.post('/api/searchs/' + id + '/toggleStatus');
	};

	searchFactory.delete = function(id) {
		return $http.delete('/api/searchs/' + id);
	};

	searchFactory.inviteFriend = function(data) {
		return $http.post('/api/invite-friend/', data);
	};

		return searchFactory;

});