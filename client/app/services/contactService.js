angular.module('contactService', [])

.factory('Contact', function($http) {

	var contactFactory = {};

	contactFactory.send = function(contactData) {
		return $http.post('client/mailer', contactData);
	};

	return contactFactory;

});