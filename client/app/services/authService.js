angular.module('authService', [])

  .factory('Auth', function ($q, $timeout, $http) {

    var user;

    return {

      userData: function() {
        return $http({method: 'GET', url: "auth/session"});
      },

      login: function (username, password) {
        // create a new instance of deferred
        var deferred = $q.defer();

        // send a post request to the server
        $http.post('/auth/login',
          {username: username, password: password})
          // handle success
          .success(function (data, status) {
            if(status === 200){
              user = true;
              deferred.resolve();
            } else {
              user = false;
              deferred.reject();
            }
          })
          // handle error
          .error(function (data) {
            user = false;
            deferred.reject();
          });

        // return promise object
        return deferred.promise;
      },

      logout: function () {
      // create a new instance of deferred
        var deferred = $q.defer();

        // send a get request to the server
        $http.get('/auth/logout')
          // handle success
          .success(function (data) {
            user = false;
            deferred.resolve();
          })
          // handle error
          .error(function (data) {
            user = false;
            deferred.reject();
          });

        // return promise object
        return deferred.promise;

      },

      register: function (user) {
        // create a new instance of deferred
        var deferred = $q.defer();

        // send a post request to the server
        $http.post('/auth/register', user)
          .success(function (data, status) {
            if(status === 200 && data.status){
              deferred.resolve();
            } else {
              deferred.reject();
            }
          })
          .error(function (data) {
            deferred.reject(data);
          });

        return deferred.promise;
      }

    };

  });