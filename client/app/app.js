angular.module('seloger', ['routerRoutes','ngTouch','ngCookies','angular.filter','appartCtrl', 'appartService', 'authCtrl','layoutCtrl','authService','searchService','likeService','searchCtrl','userCtrl','userService','headerDirective','mainDirective','contactService','angular-growl','appartFilters','rzModule','ngSanitize', 'ui.select','ngTagsInput'])

// create the controller and inject Angular's 
// this will be the controller for the ENTIRE site

.config(function(growlProvider) {
  growlProvider.globalDisableIcons(true);
  growlProvider.globalTimeToLive(5000);
  growlProvider.globalDisableCountDown(true);
})

.controller('mainController', function($scope, $rootScope, $location, growl) {
	
	$scope.isNotActive = function (viewLocation) { 
		if (viewLocation == "/") {
     		return viewLocation !== $location.path();
    	} else {
      		return $location.path().indexOf(viewLocation) == -1;
    	}
        // return viewLocation !== $location.path();
    };

})

// home page specific controller
.controller('homeController', function($scope, Contact) {

	$scope.contactForm = {};
  
    $scope.sendEmail = function () {

	    // initial values
	    $scope.error = false;
	    $scope.disabled = true;

	    // call send from service
	    Contact.send($scope.contactForm)
	      // handle success
	      .then(function () {
	        $scope.disabled = false;
	        $scope.contactForm = {};
	        $scope.form.$setPristine();
	      })
	      // handle error
	      .catch(function () {
	        $scope.error = true;
	        $scope.disabled = false;
	        $scope.contactForm = {};
	      });

    };

})

// about page controller
.controller('aboutController', function($scope) {

})

// contact page controller
.controller('contactController', function($scope, Contact) {

	$scope.contactForm = {};
  
    $scope.sendEmail = function () {

	    // initial values
	    $scope.error = false;
	    $scope.disabled = true;

	    // call send from service
	    Contact.send($scope.contactForm)
	      // handle success
	      .then(function () {
	        $scope.disabled = false;
	        $scope.contactForm = {};
	        $scope.form.$setPristine();
	      })
	      // handle error
	      .catch(function () {
	        $scope.error = true;
	        $scope.disabled = false;
	        $scope.contactForm = {};
	      });

    };

});