// inject ngRoute for all our routing needs
angular.module('routerRoutes', ['ngRoute'])

// configure our routes
.config(function($routeProvider, $locationProvider) {

    var checkLoggedin = function($q, $timeout, $http, $location){
      // Initialize a new promise
      var deferred = $q.defer();

      // Make an AJAX call to check if the user is logged in
      $http.get('/auth/session').success(function(user){
        // Authenticated
        if (user !== '0')
          deferred.resolve();

        //Not Authenticated
        else {
          deferred.reject();
          $location.url('/login');
        }
      });

      return deferred.promise;
    };

    var checkLanding = function($q, $timeout, $http, $location){
      // Initialize a new promise
      var deferred = $q.defer();

      // Make an AJAX call to check if the user is logged in
      $http.get('/auth/session').success(function(user){
        // Authenticated
        if (user == '0') {
          deferred.resolve();

        //Not Authenticated
        } else {
          deferred.reject();
          $location.url('/apparts');
        }
      });

      return deferred.promise;
    };

    var checkAdmin = function($q, $timeout, $http, $location){
      // Initialize a new promise
      var deferred = $q.defer();

      // Make an AJAX call to check if the user is logged in
      $http.get('/auth/session').success(function(user){
        // Authenticated
        if (user.admin == true)
          deferred.resolve();

        //Not Authenticated
        else {
          deferred.reject();
          $location.url('/');
        }
      });

      return deferred.promise;
    };

    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl : 'app/views/pages/home.html',
            controller  : 'homeController',
            controllerAs: 'home',
            resolve: { isloggedin: checkLanding }
        })

        // route for the about page
        .when('/about', {
            templateUrl : 'app/views/pages/about.html',
            controller  : 'aboutController',
            controllerAs: 'about',
            resolve: { isloggedin: checkLoggedin }
        })

        // route for the contact page
        .when('/contact', {
            templateUrl : 'app/views/pages/contact.html',
            controller  : 'contactController',
            controllerAs: 'contact',
            resolve: { isloggedin: checkLoggedin }
        })

        .when('/searchs', {
            templateUrl : 'app/views/pages/searchs/all.html',
            controller  : 'searchController',
            controllerAs: 'search',
            resolve: { isloggedin: checkLoggedin }
        })

        .when('/searchs/new', {
            templateUrl : 'app/views/pages/searchs/new.html',
            controller  : 'searchCreateController',
            controllerAs: 'search',
            resolve: { isloggedin: checkLoggedin }
        })

        .when('/searchs/:search_id', {
            templateUrl : 'app/views/pages/searchs/show.html',
            controller  : 'searchShowController',
            controllerAs: 'search',
            resolve: { isloggedin: checkLoggedin }
        })

        .when('/searchs/:search_id/edit', {
            templateUrl : 'app/views/pages/searchs/edit.html',
            controller  : 'searchEditController',
            controllerAs: 'search',
            resolve: { isloggedin: checkLoggedin }
        })

        .when('/apparts', {
            templateUrl : 'app/views/pages/apparts/all.html',
            controller  : 'appartController',
            controllerAs: 'appart',
            resolve: { isloggedin: checkLoggedin }
        })

        .when('/apparts/:appart_id', {
            templateUrl : 'app/views/pages/apparts/show.html',
            controller  : 'appartShowController',
            controllerAs: 'appart',
            resolve: { isloggedin: checkLoggedin }
        })

        .when('/users', {
            templateUrl : 'app/views/pages/users/all.html',
            controller  : 'userController',
            controllerAs: 'user',
            resolve: { isadmin: checkAdmin }
        })

        .when('/users/:user_id', {
            templateUrl : 'app/views/pages/users/show.html',
            controller  : 'userShowController',
            controllerAs: 'user',
            resolve: { isloggedin: checkLoggedin }
        })

        .when('/landing/like_appart', {
            templateUrl : 'app/views/pages/landing/like_appart.html'
        })

        .when('/landing/pause_search', {
            templateUrl : 'app/views/pages/landing/pause_search.html'
        })

        .when('/landing/confirm_email', {
            templateUrl : 'app/views/pages/landing/confirm_email.html'
        })

        .when('/landing/confirm_invite/:token', {
            templateUrl : 'app/views/pages/landing/confirm_invite.html'
        })

        .when('/landing/reset_password/:token', {
            templateUrl : 'app/views/pages/landing/reset_password.html'
        })
        
        .otherwise({
            redirectTo: '/'
        }); 

    $locationProvider.html5Mode(true);
});

